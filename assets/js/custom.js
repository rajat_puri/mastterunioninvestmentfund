var pieHeight = 350;
var pieWidth = 520;

// $(window).load(function() {
    
// console.log("test");


// });
// google charts starts

// Load the Visualization API and the corechart package.
google.charts.load('current', {'packages':['corechart']});

// Set a callback to run when the Google Visualization API is loaded.
// google.charts.setOnLoadCallback(drawChart);

// google.charts.setOnLoadCallback(drawPie);

// Callback that creates and populates a data table,
// instantiates the pie chart, passes in the data and
// draws it.
function drawChart() {

  // Create the data table.
  var data = new google.visualization.DataTable();
  data.addColumn('string', 'Topping');
  data.addColumn('number', 'Slices');
  data.addRows([
    ['Energy', 5],
    ['Banking & Finance', 15],
    ['Chemicals', 8],
    ['Diversified', 17],
    ['Auto Ancilaries', 10],
    ['IT', 18],
    ['Auto Ancilaries', 26],
    ['Infastructure', 35],
    ['Insurance', 17],
    ['Lubricants', 10],
    ['Packaging', 5],
    ['Paints', 5],
    ['Pharmaceuticals', 20],
    ['Real Estate', 10],
    ['Retail', 10],
    ['Diversified', 20]
  ]);

  // Set chart options
  var options = {
      'legend':'none',
      // 'title':'My Big Pie Chart',
      // 'is3D':true,
      'width':pieWidth,
      'height':pieHeight,
      'pieHole':0.6,
      colors: ['FEEEB3', 'FAD133'],
      pieSliceTextStyle: {
        color: 'black',
      }
      };

  // Instantiate and draw our chart, passing in some options.
  var chart = new google.visualization.PieChart(document.getElementById('chart_div'));
  chart.draw(data, options); 
}

function drawPie() {

    // Create the data table.
    var data = new google.visualization.DataTable();
    data.addColumn('string', 'Topping');
    data.addColumn('number', 'Slices');
    data.addRows([
      ['KEI Industries', 76],
      ['Hindalco Inds.', 35],
      ['NIIT', 30],
      ['Avenue Supermarts', 25],
      ['Infosys', 20],
    ]);
  
    // Set chart options
    var options = {
        'legend':'none',
        // 'title':'My Big Pie Chart',
        // 'is3D':true,
        'width': pieWidth,
        'height':pieHeight,
        'pieHole':0.5,
        colors: ['FAD133'],
        pieSliceTextStyle: {
          color: 'black',
        }
        };
  
    // Instantiate and draw our chart, passing in some options.
    var chart = new google.visualization.BarChart(document.getElementById('chart_pie'));
    chart.draw(data, options); 
  }


// google charts ends

$(document).ready(function(){

    if ($(window).width() < 960) {
        pieHeight = 200;
        pieWidth = 300;
     }
     else {
        pieHeight = 350;
        pieWidth = 520;
     }
     google.charts.setOnLoadCallback(drawPie);
     google.charts.setOnLoadCallback(drawChart);

    $('.tab-a').click(function(){  
        $(".tab").removeClass('tab-active');
        $(".tab[data-id='"+$(this).attr('data-id')+"']").addClass("tab-active");
        $(".tab-a").removeClass('active-a');
        $(this).addClass('active-a');
        let active = $(".tab-menu").find('li.active-a');
        scrollTab(active);
       });

    $('.ham-button').on("click", function(){
        console.log('test')
        $('.ham-button').toggleClass('active');
        $('.navbar').toggleClass('active');
    });

    // to slide active li to the start
    function scrollTab(active) {
        // let active = $(".tab-menu").find('li.active-a');

        console.log('scrolling ',active)
        var activeWidth = active.width() / 2; // get active width center
        let ancorVar = $(active).find('a').attr('href');
        let parent = active.parent().attr('id');
        var pos = active.position()?.left + activeWidth; //get left position of active li + center position
        var currentscroll = active.parent().scrollLeft(); // get current scroll position
        var divwidth = active.parent().width(); //get div width
        // pos = pos + currentscroll - divwidth / 2; // for center position if you want adjust then change this

        if (activeWidth > 60) {
            pos = pos + currentscroll - (divwidth / 2) + 60;
        }
        else {
            pos = pos + currentscroll - (divwidth / 2) + 100;
        }
        active.parent().animate({
            scrollLeft: pos
        }, 300);
    }

    

    const swiper = new Swiper('.master-swiper', {
        slidesPerView: 4,
        loop: false,
        parallax: true,
        mousewheel: false,
        speed: 1500,
        freeMode: true,
        breakpoints: {
        0: {
            slidesPerView:1.7,
            spaceBetween:15,
        },
        420: {
            slidesPerView:2,
            spaceBetween:15,
        },
    
        768: {
            slidesPerView:2.8,
            spaceBetween:15,
    
        },

        1023: {
            slidesPerView:4.5,
            spaceBetween:15,
    
        },

        1366: {
            slidesPerView:6,
            spaceBetween:15,
        }
    },
    });
    
    const muif_news_slider = new Swiper('.muif_news_slider', {
        slidesPerView: 4,
        spaceBetween: 15,
        loop: false,
        parallax: true,
        loop: false,
        mousewheel: false,
        speed: 1500,
        freeMode: true,
        navigation: {
            nextEl: ".muif_news_slider-next",
            prevEl: ".muif_news_slider-prev",
        },
        breakpoints: {
            0: {
                slidesPerView: 1.3,
                freeMode: true,
                slidesPerGroup: 1
            },
            420: {
                slidesPerView: 1.3,
                freeMode: true,
                slidesPerGroup: 1
            },
            768: {
                slidesPerView: 2.2,
                freeMode: true,
                slidesPerGroup: 1
            },
            1024: {
                slidesPerView: 4,
                freeMode: false,
                slidesPerGroup: 4
            },
            1366: {
                slidesPerView: 4,
                spaceBetween: 10,
                slidesPerGroup: 1,
                freeMode: false,
            },
        },
        });


});
